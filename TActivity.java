package com.example.nanssogong;


import java.lang.reflect.Field;

import com.example.nanssogong.R;
import android.graphics.drawable.Drawable;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;

public class TActivity extends TabActivity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.tab);
	    
	    TabWidget tw=getTabWidget();
        TabHost tabhost = getTabHost();
         
        TabHost.TabSpec spec;
        Intent intent;
        Resources res = getResources();
         
        intent = new Intent().setClass(this, MainActivity.class);
        spec = tabhost.newTabSpec("Home");
        spec.setIndicator("");
        spec.setContent(intent);
        tabhost.addTab(spec);
         
        intent = new Intent().setClass(this, DoTwit.class);
        spec = tabhost.newTabSpec("My group");
        spec.setIndicator("");
        spec.setContent(intent);
        tabhost.addTab(spec);
         
        intent = new Intent().setClass(this, SetupActivity.class);
        spec = tabhost.newTabSpec("Setup");
        spec.setIndicator("");
        spec.setContent(intent);
        tabhost.addTab(spec);
        changeTabWidgetStyle(tw);
        
	    // TODO Auto-generated method stub
	}

	 private void changeTabWidgetStyle(TabWidget tw){
	        for (int i = 0; i < tw.getChildCount(); i++) {
	            View v = tw.getChildAt(0);
	            v.setBackgroundDrawable(getResources().getDrawable(R.drawable.home32));
	            View v1 = tw.getChildAt(1);
	            v1.setBackgroundDrawable(getResources().getDrawable(R.drawable.users32));
	            View v2 = tw.getChildAt(2);
	            v2.setBackgroundDrawable(getResources().getDrawable(R.drawable.gear32));
	            
	        }
	        
	        Field mBottomLeftStrip;
	        Field mBottomMiddleStrip;
	        Field mBottomRightStrip;
	        try {
	            mBottomLeftStrip = tw.getClass().getDeclaredField("mBottomLeftStrip");
	            mBottomMiddleStrip = tw.getClass().getDeclaredField("mBottomMiddleStrip");
	            mBottomRightStrip = tw.getClass().getDeclaredField("mBottomRightStrip");
	            
	            if(!mBottomLeftStrip.isAccessible()) {
	                mBottomLeftStrip.setAccessible(true);
	            }
	            if(!mBottomMiddleStrip.isAccessible()) {
	                mBottomMiddleStrip.setAccessible(true);
	            }
	            if(!mBottomRightStrip.isAccessible()){
	                mBottomRightStrip.setAccessible(true);
	            }
	            mBottomLeftStrip.set(tw, getResources().getDrawable(R.drawable.home32));
	            mBottomMiddleStrip.set(tw, getResources().getDrawable(R.drawable.users32));
	            mBottomRightStrip.set(tw, getResources().getDrawable(R.drawable.gear32));
	            
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	 }
	 
}
