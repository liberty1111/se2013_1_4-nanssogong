package com.example.nanssogong;

import com.google.android.gms.maps.model.LatLng;

import android.app.Application;

public class MyApplication extends Application  {   //전역변수 설정을 위한 클래스
 private LatLng Position; //전역변수를 생성을 위한 변수 선언
 private String Str;
 
 public LatLng getPosition()
 {
	 return Position;
 }
 public void setPosition(LatLng globalPosition)
 {
	//this.Position.latitude=globalPosition.latitude;
	//this.Position.longitude=globalPosition.longitude;
	 this.Position=globalPosition;
 }
 public void setString(String globalString) 
 { 
   this.Str = globalString; 
 } 
 public String getString()
 {
	 return Str;
 }
 
}
