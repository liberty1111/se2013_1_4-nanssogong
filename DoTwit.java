package com.example.nanssogong;

import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class DoTwit extends Activity  {
	EditText edit;
	String lat;
	String lng;
	String link;
	
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.twitting);
		
		Button button_maptwit =(Button) findViewById(R.id.button2);
		button_maptwit.setOnClickListener(new OnClickListener() {
			public void onClick(View v)
			{
				Intent main=new Intent(DoTwit.this,googleActivity.class);
				startActivity(main);
			}
		});
		
		final LatLng position;                              //전역변수의 사용
		 MyApplication myApp=(MyApplication) getApplication();
		 position=myApp.getPosition();
		 
		edit=(EditText) findViewById(R.id.editText_link);
		
		if(position!=null)
		Location_link();
		
		 //edit.setTextKeepState(lat);
		
	}
	
	 public void Location_link()      //해당위치에 대한 정보를 링크로 만들어 주는 함수
	  {
	   Intent intent = getIntent();  // 값을 가져오는 인텐트 객체생성
	  
	  lat=intent.getExtras().getString("data_string1"); // 가져온 값을 set해주는 부분
	   lng=intent.getExtras().getString("data_string2");
        link = "<a href=\"http://maps.google.com/maps?z=18&q=" +lat+","+lng+"\""+">지도 보기</a>";
       
	  edit.setTextKeepState(Html.fromHtml(link));
	  
}
