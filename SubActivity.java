package com.example.nanssogong;

import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class SubActivity extends Activity {  //주소검색기능과 위치정보 보내기 버튼을 구현하는 클래스
	EditText edit;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sub);
		
		edit=(EditText) findViewById(R.id.edit);
		Button button_ok = (Button) findViewById(R.id.button_ok);
		
		button_ok.setOnClickListener( new OnClickListener() {  //버튼이 눌려졌을때, 등록된 텍스트를 마커의 제목으로 설정
			public void onClick(View v) {
				Intent intent=new Intent();
				intent.putExtra("INPUT_TEXT",edit.getText().toString());
				setResult(1,intent);
				finish();
			}
		});
		
		final LatLng position;
		 MyApplication myApp=(MyApplication) getApplication();
		 position=myApp.getPosition();
		 
		
		Button button_twit =(Button) findViewById(R.id.button_cancels);
		button_twit.setOnClickListener(new OnClickListener() {   //버튼이 눌려졌을때, 현재 마커를 찍은 위치의 위도와 경도값을 DoTwit클래스로 보냄
			public void onClick(View v)
			{
				Intent intent=new Intent(SubActivity.this,DoTwit.class);
				;
				intent.putExtra("data_string1",Double.toString(position.latitude));	
				intent.putExtra("data_string2",Double.toString(position.longitude));
				
				 setResult(Activity.RESULT_OK,intent);
			
				startActivity(intent);
			}
		});
	}
	
}
