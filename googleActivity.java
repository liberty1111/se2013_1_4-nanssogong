package com.example.nanssogong;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.gms.maps.GoogleMap;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener; 

public class googleActivity extends Activity {
	
	private Geocoder coder; 
    private GoogleMap mMap;
   
    
    GoogleMapOptions options = new GoogleMapOptions();
    
    String Marker_title;
    Marker myposition;
    LatLng LastPosition;
    
    String Area = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gmain);
		
		coder  = new Geocoder(this);  
		
		  mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		  //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		  mMap.setMyLocationEnabled(true);	  
		  //registerForContextMenu(mMap);	  
		  mMap.setOnMapLongClickListener(new OnMapLongClickListener() {  //지도 꾹 누를시 이벤트 설정 

				@Override
				public void onMapLongClick(LatLng point) {     
					
					Marker_title="Marker";
					
					 getAddressfromGeocode(point.latitude,point.longitude);   //위도 경도를 받아서 해당위치의 주소를 불러오는 함수
					 
					 myposition = mMap.addMarker(new MarkerOptions()    //마커 추가
			        .position(point)
			        .title(Marker_title)
			        .snippet(Area));
					
					 LastPosition=point;
					 
					final LatLng position=point;
					 MyApplication myApp=(MyApplication) getApplication();
					 myApp.setPosition(position);
					
					 mMap.setOnInfoWindowClickListener(              //마커 위 InfoWindow클릭시 이벤트
							  new OnInfoWindowClickListener(){
							    public void onInfoWindowClick(Marker marker){
							      Intent nextScreen = new Intent(googleActivity.this,SubActivity.class);  
							       // nextScreen.putExtra("userId", "" + userId);
							        //nextScreen.putExtra("eventId", "" + eventId);

							        startActivityForResult(nextScreen,0);  //SubActivity로 이동
							    }
							  }
							);
				 }
				});
	}
	@Override
	public void onActivityResult(int requestCode,int resultCode,Intent data) {  //SubActivity에서 값 받아오는 함수
		if(requestCode == 0)
		{
			if(resultCode==1)
			{
				Marker_title=data.getStringExtra("INPUT_TEXT").toString();
			    myposition.setTitle(Marker_title);
			}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {     //옵션메뉴 
		super.onCreateOptionsMenu(menu);
        MenuItem item1 = menu.add(0, 1, 0, "주소로 검색");
        SubMenu sub=menu.addSubMenu("지도 변환");
        sub.add(0,2,0, "기본");
        sub.add(0,3,0,"위성사진");
        MenuItem item2= menu.add(0,4,0,"최근 마커로 가기");
        MenuItem item3 = menu.add(0, 5, 0, "홈으로");
        return true;

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		//return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {   //옵션메뉴 리스트와 선택되었을때 이벤트 설정
		switch(item.getItemId()){
		case 1:
			Intent intent=new Intent(googleActivity.this,SearchAddress.class);
			startActivity(intent);
			return true;
		case 2:
			mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			return true;
		case 3:
			mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			return true;
		case 4:
			if(LastPosition!=null)
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LastPosition,15));
			else
				Toast.makeText(googleActivity.this, "마커를 찍고 눌러주겠니?",Toast.LENGTH_SHORT ).show();
			return true;
		default :
			return super.onContextItemSelected(item);
		}
		
	}
	public void getAddressfromGeocode(double lat, double lng)     //위도 경도를 받아서 해당위치의 주소를 불러오는 함수
	{
		 try{

		   List<Address> addresses = coder.getFromLocation( lat, lng,5 );

		   Area = addresses.get( 0 ).getCountryName();

		   

		   if (addresses.size() > 0)

		 {

		  Address mAddress = addresses.get(0);

		  Area = null;

		  StringBuffer strbuf = new StringBuffer();

		  String buf;

		  for (int i = 0; (buf = mAddress.getAddressLine(i)) != null; i++)

		  {

		   strbuf.append(buf + "\n");

		  }

		  Area = strbuf.toString();

		 

		 }

		}

		catch( Exception e ){

		   e.printStackTrace();

		} 
	}

}






